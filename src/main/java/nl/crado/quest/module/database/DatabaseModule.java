package nl.crado.quest.module.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import nl.crado.maxel.core.framework.Maxel;
import nl.crado.maxel.core.framework.annotations.LoadFirst;
import nl.crado.maxel.core.framework.module.AbstractModule;
import nl.crado.quest.framework.QuestPlayer;
import nl.crado.quest.framework.QuestPlayer.QuestPlayerBuilder;
import nl.crado.quest.framework.quest.Quest;
import nl.crado.quest.framework.quest.Quest.QuestBuilder;
import nl.crado.quest.framework.quest.QuestProgress;
import nl.crado.quest.framework.quest.task.Task;
import nl.crado.quest.framework.quest.task.Task.TaskBuilder;
import nl.crado.quest.module.quest.QuestModule;

import org.bukkit.entity.Player;

import com.zaxxer.hikari.HikariDataSource;

@LoadFirst(reason = "This is the database")
public class DatabaseModule implements AbstractModule {

	private HikariDataSource datasource;

	@Override
	public void onEnable() throws Exception {
		datasource = new HikariDataSource();
		datasource.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		datasource.addDataSourceProperty("serverName", "192.168.1.110");
		datasource.addDataSourceProperty("port", 3306);
		datasource.addDataSourceProperty("databaseName", "quests");
		datasource.addDataSourceProperty("user", "Kevin");
		datasource.addDataSourceProperty("password", "Welkom01");

		try (Connection connection = getConnection()) {
			connection.setAutoCommit(false);

			try (Statement s = connection.createStatement()) {
				/*TODO add foreign keys*/
				s.addBatch("CREATE TABLE IF NOT EXISTS quest_p ("
						+ "id BIGINT NOT NULL UNIQUE AUTO_INCREMENT, "
						+ "uuid VARCHAR(36) NOT NULL UNIQUE);");


				s.addBatch("CREATE TABLE IF NOT EXISTS quests ("
						+ "id BIGINT NOT NULL UNIQUE AUTO_INCREMENT, "
						+ "name VARCHAR(64) NOT NULL UNIQUE);");


				s.addBatch("CREATE TABLE IF NOT EXISTS quest_tasks ("
						+ "id BIGINT NOT NULL UNIQUE AUTO_INCREMENT, " /*To link to messages*/
						+ "t_id INT NOT NULL, " /*what step task at in quest*/
						+ "q_id BIGINT NOT NULL, " /*what quest it is.*/
						+ "task_type VARCHAR(32) NOT NULL, "
						/*TODO add more task details*/

						+ "CONSTRAINT UNIQUE (`t_id`, `q_id`) "
						+ ");");


				s.addBatch("CREATE TABLE IF NOT EXISTS quest_task_messages ("
						+ "q_t_id BIGINT NOT NULL, "
						+ "msg_id INT NOT NULL, "
						+ "msg VARCHAR(512) NOT NULL, "
						+ ""
						+ "CONSTRAINT FOREIGN KEY (`q_t_id`) REFERENCES quest_tasks (`id`) ON DELETE CASCADE, "
						+ "CONSTRAINT UNIQUE (`q_t_id`, `msg_id`) "
						+ ");");


				s.addBatch("CREATE TABLE IF NOT EXISTS quest_p_progress ("
						+ "p_id BIGINT NOT NULL, "
						+ "q_id BIGINT NOT NULL, "
						+ "task INT NOT NULL, "
						+ "finished BOOLEAN DEFAULT '0', "
						+ "CONSTRAINT FOREIGN KEY (`p_id`) REFERENCES quest_p (`id`) ON DELETE CASCADE, "
						+ "CONSTRAINT FOREIGN KEY (`q_id`) REFERENCES quests (`id`) ON DELETE CASCADE, "
						+ "CONSTRAINT FOREIGN KEY (`task`) REFERENCES quest_tasks (`t_id`) ON DELETE CASCADE, "
						+ "CONSTRAINT UNIQUE (`p_id`, `q_id`, `task`) "
						+ ");");


				s.executeBatch();
			}
			connection.setAutoCommit(true);
		}
	}


	public Connection getConnection() throws SQLException {
		return datasource.getConnection();
	}


	/**
	 * Only call when you know what you are doing!
	 * 
	 * @param uuid
	 * @return
	 */
	public Optional<QuestPlayer> getQuestPlayerSync(Player p) throws SQLException {
		QuestPlayer qp = null;
		try (Connection con = getConnection()) {
			QuestPlayerBuilder builder = QuestPlayer.builder();
			try(PreparedStatement ps = con.prepareStatement("SELECT * FROM quest_p WHERE uuid=?;")) {
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						builder.player(p).uuid(p.getUniqueId()).build();
					}
					else {
						/*Empty value*/
						return Optional.of(QuestPlayer.builder().player(p).uuid(p.getUniqueId()).build());
					}
				}
			}
			try(PreparedStatement ps = con.prepareStatement("SELECT q_id, task FROM quest_p_progress JOIN WHERE p_id = (SELECT id FROM quest_p WHERE uuid=?);")) {
				ps.setString(1, p.getUniqueId().toString());
				try (ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {
						Optional<Quest> oq = Maxel.getModule(QuestModule.class).getQuestByID(rs.getInt("q_id"));
						if (oq.isPresent()) {
							QuestProgress prog = QuestProgress.builder()
									.parentQuest(oq.get())
									.finished(rs.getBoolean("finished"))
									.started(true)
									.build();
							builder.quest(prog);
						}
					}
				}
			}
		}
		return Optional.ofNullable(qp);
	}

	public CompletableFuture<Optional<QuestPlayer>> getQuestPlayer(Player p) {
		return CompletableFuture.supplyAsync(new Supplier<Optional<QuestPlayer>>() {
			@Override
			public Optional<QuestPlayer> get() {
				Optional<QuestPlayer> q = Optional.empty();
				try {
					q = getQuestPlayerSync(p);
				} catch (SQLException e) {
					e.printStackTrace();
					q = Optional.empty();
				}
				return q;
			}
		});
	}

	private final String splitter = "%%&%%";
	public void loadQuests() {
		QuestModule questModule = Maxel.getModule(QuestModule.class);
		try (Connection con = getConnection()) {
			try (PreparedStatement ps = con.prepareStatement("SELECT id, name FROM quests;")) {
				try(ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {

						int id = rs.getInt("id");
						String name = rs.getString("name");
						QuestBuilder qb = Quest.builder().name(name).questDBID(id);

						try (PreparedStatement ps2 = con.prepareStatement("SELECT * FROM quest_tasks WHERE q_id = ? ORDER BY t_id ASC;")) {
							ps2.setInt(1, id);
							try(ResultSet rs2 = ps.executeQuery()) {
								while (rs.next()) {
									int taskDBID = rs2.getInt("id");
									TaskBuilder tb = Task.builder();
									tb.task(rs2.getInt("t_id")).taskDBID(taskDBID);
									try (PreparedStatement ps3 = con.prepareStatement("SELECT * FROM quest_task_messages WHERE q_t_id = ? ORDER BY msg_id ASC;")) {
										ps3.setInt(1, taskDBID);
										try(ResultSet rs3 = ps.executeQuery()) {
											StringBuilder sb = new StringBuilder();
											while (rs.next()) {
												sb.append(rs3.getString("msg") + splitter);
											}
											tb.messages(sb.toString().split(splitter));
											Task t = tb.build();
											qb.task(t.getTaskDBID(), t);
										}
									}
								}
							}
						}
						Quest quest = qb.build();
						questModule.addQuest(quest);
					}
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
}
