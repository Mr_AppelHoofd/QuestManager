package nl.crado.quest.module.quest;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import nl.crado.maxel.core.framework.datamanager.DataManager;
import nl.crado.maxel.core.framework.module.AbstractModule;
import nl.crado.quest.framework.QuestPlayer;
import nl.crado.quest.framework.quest.Quest;

public class QuestModule implements AbstractModule {

	private final String questStorage = "quests";
	private final String storageName = "p_quests";
	
	@Override
	public void onEnable() throws Exception {
		DataManager.get().setMap(storageName, new ConcurrentHashMap<>());
		DataManager.get().setMap(questStorage, new ConcurrentHashMap<>());
	}

	
	public void addQuest(Quest quest) {
		DataManager.get().addValue(questStorage, quest.getName(), quest);
	}
	
	public Optional<Quest> getQuestByName(String name) {
		return DataManager.get().getValue(questStorage, name, Quest.class);
	}
	
	public Optional<Quest> getQuestByID(int id) {
		for (Object o : DataManager.get().getMapByString(questStorage).values()) {
			if (o instanceof Quest) {
				Quest q = (Quest) o;
				if (q.getQuestDBID() == id) {
					return Optional.of(q);
				}
			}
		}
		return Optional.empty();
	}
	
	public Collection<Object> getQuests() {
		return DataManager.get().getMapByString(questStorage).values();
	}
	
	
	public Optional<QuestPlayer> getQuestPlayer(UUID uuid) {
		return DataManager.get().getValue(storageName, uuid, QuestPlayer.class);
	}
	
	public void saveQuestPlayer(QuestPlayer qp) {
		DataManager.get().addValue(storageName, qp.getUuid(), qp);
	}
	
}
