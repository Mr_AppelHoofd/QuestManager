package nl.crado.quest.module.quest.listeners;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import nl.crado.maxel.core.framework.Maxel;
import nl.crado.maxel.core.framework.datamanager.DataManager;
import nl.crado.quest.framework.QuestPlayer;
import nl.crado.quest.module.database.DatabaseModule;
import nl.crado.quest.module.quest.QuestModule;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerConnectionListener implements Listener {

	private final QuestModule questModule;
	public PlayerConnectionListener() {
		questModule  = Maxel.getModule(QuestModule.class);
	}
	
	private Set<UUID> allowedCache = new HashSet<>();
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
		if (event.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) {
			/*Niet allowed doe niks*/
		}
		else {
			allowedCache.add(event.getUniqueId());
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (event.getResult() == PlayerLoginEvent.Result.ALLOWED && allowedCache.contains(event.getPlayer().getUniqueId())) {
			Maxel.getModule(DatabaseModule.class).getQuestPlayer(event.getPlayer()).thenAccept(o -> {
				if (o.isPresent()) {
					QuestPlayer qp = o.get();
					questModule.saveQuestPlayer(qp);
					questModule.getLogger().info("Loaded player: " + qp.getPlayer().getName());
				}
				else {
					
				}
			});
		}
		allowedCache.remove(event.getPlayer().getUniqueId());
	}
	
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		
		questModule.saveQuestPlayer(null);
		
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent event) {
		/*TODO save here*/
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerQuit0(PlayerQuitEvent event) {
		DataManager.get().removeAll(event.getPlayer().getUniqueId());
	}
}
