package nl.crado.quest.framework;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import nl.crado.quest.framework.quest.QuestProgress;

import org.bukkit.entity.Player;

@Builder
public class QuestPlayer {

	@Getter private UUID uuid;
	@Getter private Player player;
	
	
	@Singular private Set<QuestProgress> quests = new HashSet<>();;
	
}
