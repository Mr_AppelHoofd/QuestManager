package nl.crado.quest.framework.events;

import lombok.Getter;
import nl.crado.quest.framework.QuestPlayer;
import nl.crado.quest.framework.quest.task.Task;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TaskStartEvent extends Event {

    @Getter private final QuestPlayer questPlayer;
    @Getter private final Task task;
    private static final HandlerList handlers = new HandlerList();

    public TaskStartEvent(QuestPlayer questPlayer, Task task) {
        this.questPlayer = questPlayer;
        this.task = task;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
