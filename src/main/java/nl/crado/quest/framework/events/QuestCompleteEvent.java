package nl.crado.quest.framework.events;

import lombok.Getter;
import nl.crado.quest.framework.quest.Quest;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class QuestCompleteEvent extends Event {

	@Getter private final Quest completedQuest;
	@Getter private final Player player;
	private static final HandlerList handlers = new HandlerList();

	public QuestCompleteEvent(Quest quest, Player p) {
		this.completedQuest = quest;
		player = p;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
