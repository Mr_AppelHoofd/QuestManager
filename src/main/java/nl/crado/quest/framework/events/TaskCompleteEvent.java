package nl.crado.quest.framework.events;

import lombok.Getter;
import nl.crado.quest.framework.quest.task.Task;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TaskCompleteEvent extends Event {

	@Getter private final Task completedTask;
	@Getter private final Player questPlayer;
	private static final HandlerList handlers = new HandlerList();

	public TaskCompleteEvent(Task task, Player questPlayer) {
		this.completedTask = task;
		this.questPlayer = questPlayer;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
