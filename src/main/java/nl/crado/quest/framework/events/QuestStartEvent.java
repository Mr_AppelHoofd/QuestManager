package nl.crado.quest.framework.events;

import lombok.Getter;
import nl.crado.quest.framework.QuestPlayer;
import nl.crado.quest.framework.quest.Quest;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class QuestStartEvent extends Event {

    @Getter private final QuestPlayer questPlayer;
    @Getter private final Quest quest;
    private static final HandlerList handlers = new HandlerList();

    public QuestStartEvent(QuestPlayer questPlayer, Quest quest) {
        this.questPlayer = questPlayer;
        this.quest = quest;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
