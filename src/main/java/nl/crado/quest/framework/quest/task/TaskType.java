package nl.crado.quest.framework.quest.task;

public enum TaskType {

    BREAK_BLOCK,
    PLACE_BLOCK,
    INTERACT_WITH_BLOCK,
    INTERACT_WITH_ENTITY,
    KILL_ENTITY,
    USE_ITEM,
    CONSUME_ITEM;

}
