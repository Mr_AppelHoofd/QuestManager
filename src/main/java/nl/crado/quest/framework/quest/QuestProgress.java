package nl.crado.quest.framework.quest;

import java.util.Optional;

import lombok.Builder;
import lombok.Getter;
import nl.crado.maxel.core.framework.Maxel;
import nl.crado.quest.framework.events.QuestCompleteEvent;
import nl.crado.quest.framework.events.TaskCompleteEvent;
import nl.crado.quest.framework.quest.task.Task;
import nl.crado.quest.module.quest.QuestModule;

import org.bukkit.entity.Player;

@Builder
public class QuestProgress {

	@Getter private Quest parentQuest;
	
	@Getter private boolean started = false;
	@Getter private boolean finished = false;
	
	@Getter private int atTask = 0;
	
	public synchronized void nextTask(Player player) {
		atTask++;
		Optional<Task> t = parentQuest.getTaskByID(atTask);
		if (t.isPresent()) {
			Maxel.getModule(QuestModule.class).callEvent(new TaskCompleteEvent(t.get(), player));
			t.get().doMessages(player);
		}
		else {
			Maxel.getModule(QuestModule.class).callEvent(new QuestCompleteEvent(parentQuest, player));
			started = false;
			finished = true;
		}
	}
}
