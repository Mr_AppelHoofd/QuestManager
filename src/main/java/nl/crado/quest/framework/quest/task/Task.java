package nl.crado.quest.framework.quest.task;

import lombok.Builder;
import lombok.Getter;
import nl.crado.maxel.core.framework.Maxel;
import nl.crado.quest.module.quest.QuestModule;

import org.bukkit.entity.Player;

@Builder
public class Task {

	@Getter private int taskDBID = -1;
	
	
	@Getter private String[] messages;
	
	private int task;
	public void doMessages(Player wrapped) {
		task = Maxel.getModule(QuestModule.class).getScheduler().scheduleSyncRepeatingTask(Maxel.getCore().getPlugin().get(), new Runnable() {
			int cur = 0;
			int max = messages.length;
			@Override
			public void run() {
				wrapped.sendMessage(messages[cur++]);
				if (cur >= max) {
					Maxel.getModule(QuestModule.class).getScheduler().cancelTask(task);
				}
			}
		}, 20, 20 * 2);
	}
}
