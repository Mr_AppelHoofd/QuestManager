package nl.crado.quest.framework.quest;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import nl.crado.quest.framework.quest.task.Task;

@Builder
public class Quest {

	@Getter private String name;
	@Getter private int questDBID;
	
	@Singular private Map<Integer, Task> tasks = new ConcurrentHashMap<>();
	
	public Optional<Task> getTaskByID(int id) {
		return Optional.ofNullable(tasks.get(id));
	}
	
}
