package nl.crado.quest;

import nl.crado.maxel.core.framework.Maxel;

import org.bukkit.plugin.java.JavaPlugin;

public class QuestPlugin extends JavaPlugin {

	@Override
	public void onEnable() {
		Maxel.getCore().setPlugin(this).setPackageName("nl.crado.quest.modules").enable();
	}
}
